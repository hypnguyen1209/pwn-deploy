FROM ubuntu:20.04

RUN dpkg --add-architecture i386

RUN apt-get update && apt-get install -y libc6:i386

COPY ./chall /

COPY cmd /tmp/cmd

# 32 bit
COPY cmd32 /tmp/cmd32

RUN chmod 777 /tmp/cmd*

RUN chmod 777 /pwn

RUN chmod 444 /flag.txt

RUN useradd ctf

USER ctf

EXPOSE 1337

CMD /tmp/cmd / :1337 ./pwn

# 32 bit
# CMD /tmp/cmd32 / :1337 ./pwn 