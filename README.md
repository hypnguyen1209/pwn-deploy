# pwn_deploy 

Deploy challenge pwnable CTF

Thêm hàm void `ignore_me_init_buffering()` chạy đầu tiên của hàm `main()`

```c
.....
void ignore_me_init_buffering() {
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);
}

int main(){
	ignore_me_init_buffering();
.....
}
```

`ELF` file được build từ gcc hoặc Makefile có standard (hoặc tương tự)

Nhét binary và flag.txt vào thư mục `chall`

Config docker theo ý muốn cũng được:

```Dockerfile
FROM ubuntu:20.04
RUN dpkg --add-architecture i386
RUN apt-get update && apt-get install -y libc6:i386
COPY ./chall /
COPY cmd /tmp/cmd
# 32 bit
COPY cmd32 /tmp/cmd32
RUN chmod 777 /tmp/cmd*
RUN chmod 777 /pwn
RUN chmod 444 /flag.txt
RUN useradd ctf
USER ctf
EXPOSE 1337
CMD /tmp/cmd / :1337 ./pwn
# 32 bit
# CMD /tmp/cmd32 / :1337 ./pwn 
```

Build and run:

```sh
docker build -t pwn-test .
docker run -d -p 13337:1337 --name=pwn-test --restart=unless-stopped pwn-test
```

Enjoy

`nc 127.0.0.1 13337`

### Thanks for [@vinhjaxt](https://github.com/vinhjaxt)